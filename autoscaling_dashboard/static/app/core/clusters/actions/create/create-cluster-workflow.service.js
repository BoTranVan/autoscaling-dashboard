/*
 *    (c) Copyright 2015 Hewlett-Packard Development Company, L.P.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  angular
    .module('horizon.cluster.clusters.actions')
    .factory('horizon.cluster.clusters.actions.create-cluster.workflow', createClusterWorkflow);

  createClusterWorkflow.$inject = [
    'horizon.cluster.clusters.actions.create-cluster.basePath',
    'horizon.app.core.workflow.factory'
  ];

  function createClusterWorkflow(autoscalingPath, dashboardWorkflow) {
    return dashboardWorkflow({
      title: gettext('Create Cluster'),

      steps: [{
        id: 'details',
        title: gettext('Details'),
        templateUrl: autoscalingPath + 'details/details.html',
        helpUrl: autoscalingPath + 'details/details.help.html',
        formName: 'createClusterDetailsForm'
      }, {
        id: 'loadbalancer',
        title: gettext('Load Balancer'),
        templateUrl: autoscalingPath + 'loadbalancer/loadbalancer.html',
        helpUrl: autoscalingPath + 'loadbalancer/loadbalancer.help.html',
        formName: 'createClusterLoadBalancerForm'
      }, {
        id: 'asPolicy',
        title: gettext('Auto Scaling Policies'),
        templateUrl: autoscalingPath + 'autoscaling-policies/autoscaling-policies.html',
        helpUrl: autoscalingPath + 'autoscaling-policies/autoscaling-policies.help.html',
        formName: 'createClusterScalingPoliciesForm'
      }],

      btnText: {
        finish: gettext('Create Cluster')
      },

      btnIcon: {
        finish: 'fa-cloud-upload'
      }
    });
  }

})();