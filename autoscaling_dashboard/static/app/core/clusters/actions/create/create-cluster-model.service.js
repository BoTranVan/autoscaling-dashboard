/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

(function() {
  'use strict';

  var push = Array.prototype.push;
  var noop = angular.noop;

  /**
   * @ngdoc overview
   * @name horizon.dashboard.project.workflow.launch-instance
   *
   * @description
   * Manage workflow of creating server.
   */

  angular
    .module('horizon.cluster.clusters.actions')
    .factory('createClusterModel', createClusterModel);

  createClusterModel.$inject = [
    '$q',
    '$log',
    'horizon.app.core.openstack-service-api.senlin',
    'horizon.app.core.openstack-service-api.lbaasv2',
    'horizon.framework.widgets.toast.service'
  ];

  /**
   * @ngdoc service
   * @name createClusterModel
   *
   * @param {Object} $q
   * @param {Object} $log
   * @param {Object} glanceAPI
   * @param {Object} novaAPI
   * @param {Object} novaExtensions
   * @param {Object} securityGroup
   * @param {Object} toast
   * @param {Object} policy
   * @description
   * This is the M part in MVC design pattern for launch instance
   * wizard workflow. It is responsible for providing data to the
   * view of each step in launch instance workflow and collecting
   * user's input from view for  creation of new instance.  It is
   * also the center point of communication between launch instance
   * UI and services API.
   * @returns {Object} The model
   */
  function createClusterModel($q, $log, senlinAPI, octaviaAPI, toast) {

    var initPromise;

    /**
     * @ngdoc model api object
     */
    var model = {
      initializing: false,
      initialized: false,

      // see initializeNewClusterSpec
      newClusterSpec: {},

      // initializeNewPolicySpec
      newPolicySpec: {
        instance_count: 1,
        interval_time: 600,
        range_time: 300
      },

      /**
       *  Name was existed
       */
      ClusterNames: [],

      /**
       * Avaliable Profiles
       */
      Profiles: [],

      LoadBalancers: [],
      LoadBalancersFullInformations: [],

      Pools: [],
      PoolsFullInformations: [],
      /**
       * This is to inform users current situation is under loading or not
       */
      loaded: {
        // Availability Profiles on Details tab
        Profiles: false,
        LoadBalancers: false,
        Pools: false,
        Clusters: false,
      },

      /**
       * api methods for UI controllers
       */
      initialize: initialize,
      createCluster: createCluster
    };

    // Local function.
    function initializeNewClusterSpec() {

      model.newClusterSpec = {
        use_load_balancer: false,
        name: '',
        profile_id: null,
        max_size: 1,
        min_size: 0,
        desired_capacity: 0,
        load_balancer_info: {
          server_group_port: 80
        },
        scale_out_info: [],
        scale_in_info: [],
      };
    }

    function initializeLoadStatus() {
      angular.forEach(model.loaded, function(val, key) {
        model.loaded[key] = false;
      });
    }

    /**
     * @ngdoc method
     * @name createClusterModel.initialize
     * @returns {promise}
     *
     * @description
     * Send request to get all data to initialize the model.
     */

    function initialize(deep) {
      var deferred, promise;

      // Each time opening launch instance wizard, we need to do this, or
      // we can call the whole methods `reset` instead of `initialize`.
      initializeNewClusterSpec();
      initializeLoadStatus();

      if (model.initializing) {
        promise = initPromise;

      } else if (model.initialized && !deep) {
        deferred = $q.defer();
        promise = deferred.promise;
        deferred.resolve();

      } else {
        model.initializing = true;

        promise = $q.all([
          senlinAPI.getClusters().then(onGetClusters, noop).finally(onGetClustersComplete),
          senlinAPI.getProfiles().then(onGetProfiles, noop).finally(onGetProfilesComplete),
          octaviaAPI.getLoadBalancers().then(onGetLoadBalancers, noop).finally(onGetLoadBalancersComplete),
          octaviaAPI.getPools().then(onGetPools, noop).finally(onGetPoolsComplete),
        ]);

        promise.then(onInitSuccess, onInitFail);
      }

      return promise;
    }


    function onInitSuccess() {
      model.initializing = false;
      model.initialized = true;
    }

    function onInitFail() {
      model.initializing = false;
      model.initialized = false;
    }

    /**
     * @ngdoc method
     * @name createClusterModel.createCluster
     * @returns {promise}
     *
     * @description
     * Send request for creating server.
     */

    function createCluster() {
      var finalSpec = angular.copy(model.newClusterSpec);

      cleanNullProperties(finalSpec);



      cleanLoadBalancer(finalSpec);
      cleanNotNeedProperties(finalSpec);

      console.log(finalSpec)
      return senlinAPI.createCluster(finalSpec).then(successMessage);
    }

    function successMessage() {
      var numberInstances = model.newClusterSpec.instance_count;
      var message = ngettext('Scheduled creation of %s cluster.',
        'Scheduled creation of %s clusters.',
        numberInstances);
      toast.add('info', interpolate(message, [numberInstances]));
    }


    // Handle finalSpec
    function cleanNotNeedProperties(finalSpec) {
      // Initially clean fields that don't have any value.
      for (var key of[]) {
        delete finalSpec[key]
      }
    }

    function cleanNullProperties(finalSpec) {
      // Initially clean fields that don't have any value.
      for (var key in finalSpec) {
        if (finalSpec.hasOwnProperty(key) && finalSpec[key] === null) {
          delete finalSpec[key];
        }
      }
    }

    function cleanLoadBalancer(finalSpec) {
      if (!finalSpec.use_load_balancer) {
        delete finalSpec.load_balancer_info
      } else {
        if (finalSpec.load_balancer_info === {}) {
          delete finalSpec.load_balancer_info
        }
      }

      delete finalSpec.use_load_balancer
    }

    // Clusters
    function onGetClusters(data) {
      model.ClusterNames.length = 0;
      push.apply(model.ClusterNames, data.data.clusters.map(function(cluster) {
        return cluster.name
      }));
    }

    function onGetClustersComplete() {
      model.loaded.Clusters = true;
    }

    // Profiles
    function onGetProfiles(data) {
      model.Profiles.length = 0;
      push.apply(model.Profiles, data.data.profiles.filter(function(profile) {
        return profile.id && profile.name && profile.os.error === undefined
      }).map(function(profile) {
        return {
          profile_id: profile.id,
          label: profile.name,
        }
      }));

      if (model.Profiles.length === 1) {
        model.newClusterSpec.profile_id = model.Profiles[0].profile_id
      } else if (model.Profiles.length > 1) {
        model.Profiles.unshift({
          label: gettext("Any Profiles"),
          profile_id: "",
        });
        model.newClusterSpec.profile_id = model.Profiles[0].profile_id;
      }
    }

    function onGetProfilesComplete() {
      model.loaded.Profiles = true;
    }


    // Load Balancer - LBaaS version 2
    function onGetLoadBalancers(data) {
      model.LoadBalancers.length = 0;
      // Save to LoadBalancersFullInformations
      push.apply(model.LoadBalancersFullInformations, data.data.items)

      push.apply(model.LoadBalancers, data.data.items.filter(function(loadbalancer) {
        return loadbalancer.id && loadbalancer.provisioning_status.indexOf("PENDING") == -1 && loadbalancer.listeners.length > 0
      }).map(function(loadbalancer) {
        return {
          id: loadbalancer.id,
          label: loadbalancer.name
        }
      }))

      if (model.LoadBalancers.length === 1) {
        model.newClusterSpec.load_balancer_info.id = model.LoadBalancers[0].id
      } else if (model.LoadBalancers.length > 1) {
        model.LoadBalancers.unshift({
          label: gettext("Any Load Balancers"),
          id: "",
        });
        model.newClusterSpec.load_balancer_info.id = model.LoadBalancers[0].id;
      }
    }

    function onGetLoadBalancersComplete() {
      model.loaded.LoadBalancers = true
    }

    function onGetPools(data) {
      model.Pools.length = 0;
      // Save to PoolsFullInformations
      push.apply(model.PoolsFullInformations, data.data.items)

      push.apply(model.Pools, data.data.items.filter(function(pool) {
        return pool.id && pool.provisioning_status.indexOf("PENDING") == -1 && pool.listeners.length > 0
      }).map(function(pool) {
        return {
          id: pool.id,
          label: pool.name
        }
      }))

      if (model.Pools.length === 1) {
        model.newClusterSpec.load_balancer_info.server_group_id = model.Pools[0].id
      } else if (model.Pools.length > 1) {
        model.Pools.unshift({
          label: gettext("Any Load Balancer Pools"),
          id: "",
        });
        model.newClusterSpec.load_balancer_info.server_group_id = model.Pools[0].id;
      }
    }

    function onGetPoolsComplete() {
      model.loaded.Pools = true
    }

    return model;
  }

})();
