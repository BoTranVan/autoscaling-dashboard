/*
 *    (c) Copyright 2015 Hewlett-Packard Development Company, L.P.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  angular
    .module('horizon.cluster.clusters.actions')
    .config(config)
    .constant('horizon.cluster.clusters.actions.create-cluster.modal-spec', {
      backdrop: 'static',
      size: 'lg',
      controller: 'ModalContainerController',
      template: '<wizard class="wizard" ng-controller="CreateClusterWizardController"></wizard>'
    })

  config.$inject = [
    '$provide',
    '$windowProvider'
  ];

  /**
   * @name config
   * @param {Object} $provide
   * @param {Object} $windowProvider
   * @description Base path for the launch-instance code
   * @returns {undefined} No return value
   */
  function config($provide, $windowProvider) {
    var autoscalingPath = $windowProvider.$get().STATIC_URL + 'app/core/clusters/actions/create/';
    $provide.constant('horizon.cluster.clusters.actions.create-cluster.basePath', autoscalingPath);
  }
})();