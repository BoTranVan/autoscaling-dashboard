/*
 * Copyright 2015 Hewlett Packard Enterprise Development Company LP
 * (c) Copyright 2015 ThoughtWorks Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name CreateClusterDetailsController
   * @description
   * The `CreateClusterDetailsController` controller provides functions for
   * configuring the details step of the Launch Instance Wizard.
   *
   */
  angular
    .module('horizon.cluster.clusters.actions')
    .controller('CreateClusterDetailsController', CreateClusterDetailsController);

  CreateClusterDetailsController.$inject = ['$scope'];

  function CreateClusterDetailsController($scope) {

    var ctrl = this;

    ctrl.isDescriptionSupported = false;
    ctrl.clusterNameInvalid = false;

    // Error text for invalid fields
    ctrl.clusterNameErrorMessage = gettext('A name is required for your cluster.');
    ctrl.instanceCountError = gettext(
      'Instance count is required and must be an integer of at least 1'
    );

    // Init value
    ctrl.maxInstanceCount = 999

    function clusterNameExisted(clusterName) {
      if ($scope.model.ClusterNames.indexOf(clusterName) !== -1) {
        return true
      }
      return false
    }

    ctrl.onchangeClusterName = function() {
      if (clusterNameExisted($scope.model.newClusterSpec.name)) {
        ctrl.clusterNameInvalid = true
        ctrl.clusterNameErrorMessage = gettext('This name was existed.');
      } else if ($scope.model.newClusterSpec.name === undefined) {
        ctrl.clusterNameInvalid = true
        ctrl.clusterNameErrorMessage = gettext('A name is required for your cluster.');
      } else {
        ctrl.clusterNameInvalid = false
      }
    }

    ctrl.ValidateSize = function() {
      if ($scope.model.newClusterSpec.desired_capacity > $scope.model.newClusterSpec.max_size || $scope.model.newClusterSpec.desired_capacity < $scope.model.newClusterSpec.min_size) {
        ctrl.clusterSizeError = gettext('Desired Capacity of cluster is invalid');
      } else {
        ctrl.clusterSizeError = ""
      }
    }
  }
})();
