/*
 *    (c) Copyright 2015 Hewlett-Packard Development Company, L.P.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  angular
    .module('horizon.cluster.clusters.actions')
    .controller('UpdateClusterWizardController', UpdateClusterWizardController);

  UpdateClusterWizardController.$inject = [
    '$scope',
    'updateClusterModel',
    'horizon.cluster.clusters.actions.update-cluster.workflow'
  ];

  function UpdateClusterWizardController($scope, updateClusterModel, updateClusterWorkflow) {
    // Note: we set these attributes on the $scope so that the scope inheritance used all
    // through the launch instance wizard continues to work.
    $scope.workflow = updateClusterWorkflow; // eslint-disable-line angular/controller-as
    $scope.model = updateClusterModel; // eslint-disable-line angular/controller-as
    $scope.model.initialize(true, $scope.cluster_id);
    $scope.submit = $scope.model.updateCluster; // eslint-disable-line angular/controller-as
  }

})();