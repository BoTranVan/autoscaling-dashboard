/*
 * Copyright 2015 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name UpdateClusterAutoScalingPoliciesController
   * @description
   * The `UpdateClusterAutoScalingPoliciesController` controller provides functions for
   * configuring the source step of the Launch Instance Wizard.
   *
   */
  var push = [].push;

  angular
    .module('horizon.cluster.clusters.actions')
    .controller('UpdateClusterAutoScalingPoliciesController', UpdateClusterAutoScalingPoliciesController);

  UpdateClusterAutoScalingPoliciesController.$inject = [
    '$scope',
    '$interval',
    'horizon.app.core.openstack-service-api.senlin'
  ];

  function UpdateClusterAutoScalingPoliciesController($scope, $interval, senlinAPI) {

    var ctrl = this;

    // toggle button label/value defaults
    ctrl.toggleButtonOptions = [{
      label: gettext('Yes'),
      value: true
    }, {
      label: gettext('No'),
      value: false
    }];

    ctrl.use_autoscaling_policies = false;

    // Show in tables
    ctrl.MetricAutoScalingText = {
      cpu_used: gettext("Avg CPU usage percentage"),
      ram_used: gettext("Avg CPU usage percentage"),
      net_used: gettext("Avg Network usage percentage"),
      request_per_second: gettext("Avg Req/s of Load Balancer")
    }

    ctrl.MetricAutoScaling = [{
      label: gettext("Any Metric Types"),
      value: ""
    }, {
      label: gettext("Avg CPU usage percentage"),
      value: "cpu_used"
    }, {
      label: gettext("Avg RAM usage percentage"),
      value: "ram_used"
    }, {
      label: gettext("Avg Network usage percentage"),
      value: "net_used"
    }];
    $scope.model.newPolicySpec.metric_type = ctrl.MetricAutoScaling[0].value


    ctrl.eventScaling = [{
      label: gettext("Any Policy Types"),
      value: ""
    }, {
      label: gettext("Do scale in cluster."),
      value: "CLUSTER_SCALE_IN"
    }, {
      label: gettext("Do scale out cluster."),
      value: "CLUSTER_SCALE_OUT"
    }]
    $scope.model.newPolicySpec.policy_type = ctrl.eventScaling[0].value

    // Instance count
    ctrl.minInstanceError = gettext('Instance count is required and must be at least 1');
    ctrl.InstanceError = gettext('Instance count is required and must be at most 10');

    // Threshold
    ctrl.checkThreshold = function() {
      if (['cpu_used', 'ram_used'].indexOf($scope.model.newPolicySpec.metric_type) !== -1) {
        ctrl.thresholdMax = 100
        ctrl.thresholdMin = 5
        ctrl.minThresholdError = gettext('Threshold is required and must be at least 5 %');
        ctrl.ThresholdError = gettext('Threshold is required and must be at least 100 %');
      }
      if (['net_used'].indexOf($scope.model.newPolicySpec.metric_type) !== -1) {
        ctrl.thresholdMin = 10
        ctrl.minThresholdError = gettext('Threshold is required and must be at least 10 MBps');
        ctrl.ThresholdError = gettext('Threshold is required and must be at least 10 MBps');
      }
      if (['request_per_second'].indexOf($scope.model.newPolicySpec.metric_type) !== -1) {
        ctrl.thresholdMin = 100
        ctrl.minThresholdError = gettext('Threshold is required and must be at least 100 req/s');
        ctrl.ThresholdError = gettext('Threshold is required and must be at least 100 req/s');
      }

    }

    // Range time
    ctrl.minRangeTimeError = gettext('Range time is required and must be at least 300 seconds');
    ctrl.RangeTimeError = gettext('Range time is required.');

    // Interval Time
    ctrl.minIntervalTimeError = gettext('Interval time is required and must be at least 300 seconds');
    ctrl.IntervalTimeError = gettext('Interval time is required.');

    // Duplicate message
    ctrl.scaleOutPoliciesError = false
    ctrl.scaleInPoliciesError = false

    // Recent update informations $scope.model.newClusterSpec.user_load_balancer
    $interval(function() {
      if (!$scope.model.newClusterSpec.use_load_balancer) {
        // Do delete autoscaling policies
        removePolicies()
      }
    }, 360);

    // Delete policies using LB if LB not used
    function removePolicies() {
      for (var policy_index in $scope.model.newClusterSpec.scale_out_info) {
        if ($scope.model.newClusterSpec.scale_out_info[policy_index].metric === "request_per_second") {
          $scope.model.newClusterSpec.scale_out_info.splice(policy_index)
        }
      }

      for (var policy_index in $scope.model.newClusterSpec.scale_in_info) {
        if ($scope.model.newClusterSpec.scale_in_info[policy_index].metric === "request_per_second") {
          $scope.model.newClusterSpec.scale_in_info.splice(policy_index)
        }
      }
    }

    // Cluster was using load balancer?
    ctrl.checkUseLoadBalancer = function() {
      if ($scope.model.newClusterSpec.use_load_balancer) {
        if (ctrl.MetricAutoScaling.length <= 4) {
          ctrl.MetricAutoScaling.push({
            label: gettext("Avg Req/s of Load Balancer"),
            value: "request_per_second"
          })
        }
      } else {
        // remove request_per_second
        if (ctrl.MetricAutoScaling.length > 4) {
          ctrl.MetricAutoScaling.pop()
        }
      }
    }

    function getLoadBalancer() {
      return $scope.model.LoadBalancersFullInformations.filter(function(lb) {
        return lb.id == $scope.model.newClusterSpec.load_balancer_info.id && $scope.model.newClusterSpec.use_load_balancer
      })[0]
    }

    function getPool() {
      return $scope.model.PoolsFullInformations.filter(function(pool) {
        return pool.id == $scope.model.newClusterSpec.load_balancer_info.server_group_id && $scope.model.newClusterSpec.use_load_balancer
      })[0]
    }

    // Make payload information to do create scaling policies with load balancer
    function makeLoadBalancerScalingPolicy() {
      return {
        "metric": "request_per_second",
        "range_time": $scope.model.newPolicySpec.range_time,
        "number": $scope.model.newPolicySpec.instance_count,
        "threshold": $scope.model.newPolicySpec.threshold,
        "cooldown": $scope.model.newPolicySpec.interval_time,
        "load_balancers": {
          "load_balancer_id": getLoadBalancer().id,
          "target_id": getPool().id,
          "target_type": "backend",
          "load_balancer_name": getLoadBalancer().name,
          "target_name": getPool().name
        },
        "event": $scope.model.newPolicySpec.policy_type
      }
    }

    // Make payload information to do create scaling policies
    function makeScalingPolicy() {
      return {
        "metric": $scope.model.newPolicySpec.metric_type,
        "range_time": $scope.model.newPolicySpec.range_time,
        "number": $scope.model.newPolicySpec.instance_count,
        "threshold": $scope.model.newPolicySpec.threshold,
        "cooldown": $scope.model.newPolicySpec.interval_time,
        "event": $scope.model.newPolicySpec.policy_type
      }
    }

    // Filter autoscaling policies
    function getDuplicatePolicies(policices, newPolicyType) {
      var wasCreated = policices.map(function(policy) {
        return policy.metric
      })

      if (wasCreated.indexOf(newPolicyType) !== -1) {
        return true
      }
      return false
    }


    // Validate model.newPolicySpec
    function newPolicySpecIsValid(payload) {
      if (payload.metric === "" || payload.event === "") {
        return false
      }

      if (payload.number < 1 || payload.number > 10) {
        return false
      }

      if (payload.cooldown < 300 || payload.range_time < 300) {
        return false
      }

      if (payload.threshold === undefined) {
        return false
      } else if (['cpu_used', 'ram_used'].indexOf(payload.metric_type) !== -1 && (payload.threshold === "" || payload.threshold < 5 || payload.threshold > 100)) {
        return false
      } else if (['request_per_second'].indexOf(payload.metric_type) !== -1 && (payload.threshold === "" || payload.threshold < 100)) {
        return false
      } else if (['net_used'].indexOf(payload.metric_type) !== -1 && (payload.threshold === "" || payload.threshold < 10)) {
        return false
      }

      return true
    }

    // Add policy informations
    ctrl.AddPolicy = function() {
      if (['request_per_second'].indexOf($scope.model.newPolicySpec.metric_type) !== -1) {
        var payload = makeLoadBalancerScalingPolicy()
      } else {
        var payload = makeScalingPolicy()
      }

      if (!newPolicySpecIsValid(payload)) {
        return
      }

      if ($scope.model.newPolicySpec.policy_type === "CLUSTER_SCALE_OUT") {
        if (!getDuplicatePolicies($scope.model.newClusterSpec.scale_out_info, payload.metric)) {
          $scope.model.newClusterSpec.scale_out_info.push(angular.copy(payload))
        } else {
          ctrl.scaleOutPoliciesError = true
          return
        }
      } else if ($scope.model.newPolicySpec.policy_type === "CLUSTER_SCALE_IN") {
        if (!getDuplicatePolicies($scope.model.newClusterSpec.scale_in_info, payload.metric)) {
          $scope.model.newClusterSpec.scale_in_info.push(angular.copy(payload))
        } else {
          ctrl.scaleInPoliciesError = true
          return
        }
      }

      ctrl.scaleOutPoliciesError = false
      ctrl.scaleInPoliciesError = false
      senlinAPI.createAutoscalingPolicy($scope.model.cluster_id, payload)
    }

    ctrl.removeItem = function(policies, index) {
      var policy = policies[index]
      policies.splice(index, 1);

      if (policy.id !== undefined) {
        senlinAPI.deleteAutoscalingPolicy($scope.model.cluster_id, policy.id)
      }
    }
  }
})();
