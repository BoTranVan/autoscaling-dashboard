/*
 * Copyright 2015 Hewlett Packard Enterprise Development Company LP
 * (c) Copyright 2015 ThoughtWorks Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name UpdateClusterDetailsController
   * @description
   * The `UpdateClusterDetailsController` controller provides functions for
   * configuring the details step of the Launch Instance Wizard.
   *
   */
  angular
    .module('horizon.cluster.clusters.actions')
    .controller('UpdateClusterDetailsController', UpdateClusterDetailsController);

  UpdateClusterDetailsController.$inject = ['$scope'];

  function UpdateClusterDetailsController($scope) {

    var ctrl = this;

    ctrl.isDescriptionSupported = false;

    // Error text for invalid fields
    ctrl.clusterNameError = gettext('A name is required for your cluster.');
    ctrl.instanceCountError = gettext(
      'Instance count is required and must be an integer of at least 1'
    );

    // Init value
    ctrl.maxInstanceCount = 999


    ctrl.ValidateSize = function() {
      if ($scope.model.newClusterSpec.desired_capacity > $scope.model.newClusterSpec.max_size || $scope.model.newClusterSpec.desired_capacity < $scope.model.newClusterSpec.min_size) {
        ctrl.clusterSizeError = gettext('Desired Capacity of cluster is invalid');
      } else {
        ctrl.clusterSizeError = ""
      }
    }
  }
})();
