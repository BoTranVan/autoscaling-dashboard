/*
 * Copyright 2015 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name UpdateClusterLoadBalancerController
   * @description
   * The `UpdateClusterLoadBalancerController` controller provides functions for
   * configuring the source step of the Launch Instance Wizard.
   *
   */

  angular
    .module('horizon.cluster.clusters.actions')
    .controller('UpdateClusterLoadBalancerController', UpdateClusterLoadBalancerController);

  UpdateClusterLoadBalancerController.$inject = ['$scope'];

  function UpdateClusterLoadBalancerController($scope) {
    var ctrl = this

    ctrl.ServerGroupPortError = gettext('Port of Instance is required.');
    ctrl.minServerGroupPortError = gettext(
      'Port of Instance is required and must be an integer of at least 22'
    );
    // toggle button label/value defaults
    ctrl.toggleButtonOptions = [{
      label: gettext('Yes'),
      value: true
    }, {
      label: gettext('No'),
      value: false
    }];
  }
})();
