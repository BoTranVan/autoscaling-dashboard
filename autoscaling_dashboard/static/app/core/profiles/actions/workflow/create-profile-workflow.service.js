/*
 *    (c) Copyright 2015 Hewlett-Packard Development Company, L.P.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular
    .module('horizon.cluster.profiles.actions')
    .factory('horizon.cluster.profiles.actions.create-profile.workflow', createProfileWorkflow);

  createProfileWorkflow.$inject = [
    'horizon.dashboard.project.workflow.launch-instance.basePath',
    'horizon.cluster.profiles.actions.create-profile.basePath',
    'horizon.app.core.workflow.factory'
  ];

  function createProfileWorkflow(basePath, autoscalingPath, dashboardWorkflow) {
    return dashboardWorkflow({
      title: gettext('Create Profile'),

      steps: [
        {
          id: 'details',
          title: gettext('Details'),
          templateUrl: autoscalingPath + 'details/details.html',
          helpUrl: autoscalingPath + 'details/details.help.html',
          formName: 'createProfileDetailsForm'
        },
        {
          id: 'source',
          title: gettext('Source'),
          templateUrl: autoscalingPath + 'source/source.html',
          helpUrl: autoscalingPath + 'source/source.help.html',
          formName: 'createProfileSourceForm'
        },
        {
          id: 'volumes',
          title: gettext('Volumes'),
          templateUrl: autoscalingPath + 'volumes/volumes.html',
          helpUrl: autoscalingPath + 'volumes/volumes.help.html',
          formName: 'createProfileVolumesForm'
        },
        {
          id: 'flavor',
          title: gettext('Flavor'),
          templateUrl: autoscalingPath + 'flavor/flavor.html',
          helpUrl: autoscalingPath + 'flavor/flavor.help.html',
          formName: 'createProfileFlavorForm'
        },
        {
          id: 'secgroups',
          title: gettext('Security Groups'),
          templateUrl: autoscalingPath + 'security-groups/security-groups.html',
          helpUrl: autoscalingPath + 'security-groups/security-groups.help.html',
          formName: 'createProfileAccessAndSecurityForm',
          requiredServiceTypes: ['network']
        },
        {
          id: 'keypair',
          title: gettext('Key Pair'),
          templateUrl: autoscalingPath + 'keypair/keypair.html',
          helpUrl: autoscalingPath + 'keypair/keypair.help.html',
          formName: 'createProfileKeypairForm',
          novaExtension: 'Keypairs'
        },
        {
          id: 'configuration',
          title: gettext('Configuration'),
          templateUrl: basePath + 'configuration/configuration.html',
          helpUrl: basePath + 'configuration/configuration.help.html',
          formName: 'launchInstanceConfigurationForm'
        },
        {
          id: 'metadata',
          title: gettext('Metadata'),
          templateUrl: basePath + 'metadata/metadata.html',
          helpUrl: basePath + 'metadata/metadata.help.html',
          formName: 'launchInstanceMetadataForm'
        }
      ],

      btnText: {
        finish: gettext('Create Profile')
      },

      btnIcon: {
        finish: 'fa-cloud-upload'
      }
    });
  }

})();
