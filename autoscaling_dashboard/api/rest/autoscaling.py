# Copyright 2015 IBM Corp.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""API over the neutron LBaaS v2 service.
"""


from django.views import generic


from openstack_dashboard.api.rest import urls
from openstack_dashboard.api.rest import utils as rest_utils

from autoscaling_dashboard.api import senlin
from autoscaling_dashboard.api import autoscaling as api_autoscaling


@urls.register
class Profiles(generic.View):
    """API for profiles.

    """
    url_regex = r'senlin/profiles/$'

    @rest_utils.ajax()
    def get(self, request):
        try:
            profiles = api_autoscaling.get_profiles(request)
            return profiles
        except Exception as e:
            raise e

    @rest_utils.ajax()
    def post(self, request):
        try:
            spec = {
                "name": request.DATA.get("name"),
                "spec": {
                    "type": "vccloud.server",
                    "version": "1.0",
                    "properties": request.DATA
                }
            }
            del spec["spec"]["properties"]["name"]
            profile = senlin.profile_create(request, **spec)
            return profile
        except Exception as e:
            raise e


@urls.register
class Clusters(generic.View):
    """API for clusters.

    """
    url_regex = r'senlin/clusters/$'

    @rest_utils.ajax()
    def get(self, request):
        try:
            cluster = api_autoscaling.get_clusters(request)
            return cluster
        except Exception as e:
            raise e

    @rest_utils.ajax()
    def post(self, request):
        try:
            cluster = api_autoscaling.create_cluster(request, request.DATA)
            return cluster
        except Exception as e:
            raise e


@urls.register
class Cluster(generic.View):
    """API for cluster.

    """
    url_regex = r'senlin/clusters/(?P<cluster_id>[^/]+)/$'

    @rest_utils.ajax()
    def get(self, request, cluster_id):
        try:
            cluster = api_autoscaling.get_cluster(request, cluster_id)
            return cluster
        except Exception as e:
            raise e

    @rest_utils.ajax()
    def delete(self, request, cluster_id):
        try:
            cluster = api_autoscaling.delete_cluster(request, cluster_id)
            return cluster
        except Exception as e:
            raise e

    @rest_utils.ajax()
    def put(self, request, cluster_id):
        try:
            cluster = api_autoscaling.update_cluster(
                request, cluster_id, request.DATA)
            return cluster
        except Exception as e:
            raise e


@urls.register
class AutoscalingPolicies(generic.View):
    """API for clusters.

    """
    url_regex = r'senlin/clusters/(?P<cluster_id>[^/]+)/policies/$'

    @rest_utils.ajax()
    def post(self, request, cluster_id):
        try:
            policy = api_autoscaling.create_autoscaling_policy(
                request, cluster_id, request.DATA)
            return policy
        except Exception as e:
            raise e


@urls.register
class AutoscalingPolicie(generic.View):
    """API for cluster.

    """
    url_regex = r'senlin/clusters/(?P<cluster_id>[^/]+)' + \
        r'/policies/(?P<policy_id>[^/]+)/$'

    @rest_utils.ajax()
    def delete(self, request, cluster_id, policy_id):
        try:
            policy = api_autoscaling.delete_autoscaling_policy(
                request, cluster_id, policy_id)
            return policy
        except Exception as e:
            raise e
