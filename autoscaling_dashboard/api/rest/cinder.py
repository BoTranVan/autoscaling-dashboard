# Copyright 2015 IBM Corp.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""API over the neutron LBaaS v2 service.
"""


from django.views import generic


from openstack_dashboard.api.rest import urls
from openstack_dashboard.api.rest import utils as rest_utils

from openstack_dashboard.api import cinder


@urls.register
class VolumeSnapshot(generic.View):
    """API for getting snapshots metadata"""
    url_regex = r'cinder/volumesnapshots/' \
                r'(?P<volume_snapshot_id>[^/]+)/$'

    @rest_utils.ajax()
    def get(self, request, volume_snapshot_id):
        """Get a specific volumes snapshot

        http://localhost/api/cinder/volumesnapshots/1/
        """
        result = cinder.volume_snapshot_get(
            request, volume_snapshot_id). to_dict()
        return result
