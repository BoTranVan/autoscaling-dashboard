from django.conf import settings


from horizon import exceptions
from horizon.utils import memoized
from keystoneauth1 import session as ks_session
from keystoneauth1.identity import generic
from openstack_dashboard.api import base
from senlinclient.v1 import client as senlin_client


from autoscaling_dashboard.api.session import get_session

USER_AGENT = 'python-autoscalingclient'


def get_autoscaling_path_resources(request):
    return get_autoscaling(request) + '/groups'


def get_autoscaling_path_items(request, cluster_id):
    return get_autoscaling(request) + '/groups/%s' % cluster_id


def get_autoscaling_path_policies_resources(request, cluster_id):
    return get_autoscaling(request) + '/groups/%s/policies' % cluster_id


def get_autoscaling_path_policies_items(request, cluster_id, policy_id):
    return get_autoscaling(request) + \
        '/groups/%s/policies/%s' % (cluster_id, policy_id)


def get_profile_resources(request):
    return get_autoscaling(request) + '/launch_configs'


def get_autoscaling(request):
    try:
        autoscaling_url = base.url_for(request, 'bfc-autoscaling')
    except exceptions.ServiceCatalogException:
        autoscaling_url = 'https://manage.bizflycloud.vn/auto-scaling/api'
    return autoscaling_url


@get_session
@memoized.memoized
def autoscalingclient(request, **kwargs):
    try:
        return kwargs.get('session')
    except Exception as e:
        raise e


@memoized.memoized
def senlinclient(request):
    auth = generic.Token(
        auth_url=getattr(settings, 'OPENSTACK_KEYSTONE_URL'),
        token=request.user.token.id,
        project_id=request.user.tenant_id
    )
    session = ks_session.Session(auth=auth)
    return senlin_client.Client(session=session,
                                region_name=request.user.services_region)

# Clusters


def get_clusters(request):
    """Get a clusters."""
    return {
        "clusters": [
            cluster.to_dict() for cluster in senlinclient(request).clusters()
        ]
    }


def create_cluster(request, params):
    """Create a cluster."""
    url = get_autoscaling_path_resources(request)
    return autoscalingclient(request).post(url, json=params).json()


def update_cluster(request, cluster_id, params):
    """Create a cluster."""
    url = get_autoscaling_path_items(request, cluster_id)
    return autoscalingclient(request).put(url, json=params).json()


def get_cluster(request, cluster_id):
    """Get a cluster."""
    url = get_autoscaling_path_items(request, cluster_id)
    return autoscalingclient(request).get(url).json()


def delete_cluster(request, cluster_id):
    """Delete a cluster."""
    url = get_autoscaling_path_items(request, cluster_id)
    return autoscalingclient(request).delete(url).json()


# Profiles
def get_profiles(request):
    """Create a cluster."""
    url = get_profile_resources(request)
    return autoscalingclient(request).get(url + "?all").json()


# Policies
def create_autoscaling_policy(request, cluster_id, params):
    url = get_autoscaling_path_policies_resources(request, cluster_id)
    return autoscalingclient(request).post(url, json=params).json()


def delete_autoscaling_policy(request, cluster_id, policy_id):
    url = get_autoscaling_path_policies_items(request, cluster_id, policy_id)
    return autoscalingclient(request).delete(url).json()
