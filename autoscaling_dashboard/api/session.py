from requests import Session


def get_session(function):

    def wrapper(request, **kwargs):
        session = Session()
        headers = {
            'X-Auth-Token': request.user.token.id,
            'X-Tenant-Name': request.user.tenant_name,
            'X-Tenant-Id': request.user.tenant_id,
            'Content-Type': 'application/json'
        }
        session.headers.update(headers)
        kwargs["session"] = session
        return function(request, **kwargs)

    return wrapper
